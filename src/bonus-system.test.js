import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

describe('Bonus system tests', () => {
    let app;
    console.log("Tests started");

    test('Invalid program test', (done) => {
        let bonus = calculateBonuses('nonsense', 1000);
        assert.equal(bonus, 0);
        done();
    });

    test('Test Standard', (done) => {
        let bonus = calculateBonuses('Standard', 1000);
        assert.equal(bonus, 0.05);
        done();
    });

    test('Test Premium', (done) => {
        let bonus = calculateBonuses('Premium', 1000);
        assert.equal(bonus, 0.05*2);
        done();
    });

    test('Test Diamond', (done) => {
        let bonus = calculateBonuses('Diamond', 1000);
        assert.equal(bonus, 0.05*4);
        done();
    });

    test('bonus 1 boundary test', (done) => {
        let bonus = calculateBonuses('Standard', 10000);
        assert.notEqual(bonus, 0.05);
        done();
    });

    test('bonus 2 test', (done) => {
        let bonus = calculateBonuses('Standard', 20000);
        assert.equal(bonus, 0.05 * 1.5);
        done();
    });

    test('bonus 2 boundary test', (done) => {
        let bonus = calculateBonuses('Standard', 50000);
        assert.notEqual(bonus, 0.05 * 1.5);
        done();
    });

    test('bonus 3 test', (done) => {
        let bonus = calculateBonuses('Standard', 60000);
        assert.equal(bonus, 0.05 * 2);
        done();
    });

    test('bonus 3 boundary test', (done) => {
        let bonus = calculateBonuses('Standard', 100000);
        assert.notEqual(bonus, 0.05 * 2);
        done();
    });

    test('bonus 4 test', (done) => {
        let bonus = calculateBonuses('Standard', 110000);
        assert.equal(bonus, 0.05 * 2.5);
        done();
    });

    console.log('Tests Finished');
});
